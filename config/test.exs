use Mix.Config

# We don't run a server during test. If one is required,
# you can enable the server option below.
config :book_search, BookSearch.Endpoint,
  http: [port: 4001],
  server: false

# Print only warnings and errors during test
#config :logger, level: :warn

# Configure your database
config :book_search, BookSearch.Repo,
  adapter: Ecto.Adapters.MySQL,
  username: "root",
  password: "password",
  database: "book_search_test",
  hostname: "localhost",
  pool: Ecto.Adapters.SQL.Sandbox

  config :logger,
    level: :info,
    backends: [:console],
    compile_time_purge_level: :debug
