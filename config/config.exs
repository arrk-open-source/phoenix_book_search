# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.
use Mix.Config

# General application configuration
config :book_search,
  ecto_repos: [BookSearch.Repo]

# Configures the endpoint
config :book_search, BookSearch.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "mWL3VZMpk6vkSN5zZW5+zw1w/+VkmFLAANAuHAkkfLY5AVcQRgJrievhdT0c7HKR",
  render_errors: [view: BookSearch.ErrorView, accepts: ~w(html json)],
  pubsub: [name: BookSearch.PubSub,
           adapter: Phoenix.PubSub.PG2]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env}.exs"
