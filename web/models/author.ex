defmodule BookSearch.Author do
  use BookSearch.Web, :model

  schema "authors" do
    field :name, :string
    field :email, :string
    has_many :books, BookSearch.Book

    timestamps()
  end

  @doc """
  Builds a changeset based on the `struct` and `params`.
  """
  def changeset(struct, params \\ %{}) do
    struct
    |> cast(params, [:name, :email])
    |> validate_required([:name])
    |> validate_length(:name, min: 2)
    |> validate_length(:name, max: 30)
    |> validate_length(:email, min: 5)
    |> validate_length(:email, max: 30)
    |> validate_format(:email, ~r/@/)
  end
end
