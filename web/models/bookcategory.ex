defmodule BookSearch.BookCategory do
  use BookSearch.Web, :model

  schema "bookcategories" do
    belongs_to :book, BookSearch.Book
    belongs_to :category, BookSearch.Category
  end

  @doc """
  Builds a changeset based on the `struct` and `params`.
  """
  def changeset(struct, params \\ %{}) do
    struct
    |> cast(params, [:book_id, :category_id])
    |> validate_required([])
  end
end
