defmodule BookSearch.Book do
  use BookSearch.Web, :model
  alias BookSearch.{Repo, Category}

  schema "books" do
    field :title, :string
    field :isbn, :string
    field :year, :integer
    many_to_many :categories, BookSearch.Category, join_through: "bookcategories", on_replace: :delete
    belongs_to :author, BookSearch.Author

    timestamps()
  end

  @doc """
  Builds a changeset based on the `struct` and `params`.
  """
  def changeset(struct, params \\ %{}) do
    struct
    |> cast(params, [:title, :isbn, :year, :author_id])
    |> validate_required([:title, :isbn, :year])
    |> put_assoc(:categories, parse_category_ids(params))
  end

  defp parse_category_ids(params) do
    (params["category_ids"] || [])
    |> Enum.map(&get_category/1)
  end

  defp get_category(id) do
      Repo.get_by(Category, id: id)
  end
end
