defmodule BookSearch.Category do
  use BookSearch.Web, :model

  schema "categories" do
    field :name, :string
    many_to_many :books, BookSearch.Book, join_through: "bookcategories"

    timestamps()
  end

  @doc """
  Builds a changeset based on the `struct` and `params`.
  """
  def changeset(struct, params \\ %{}) do
    struct
    |> cast(params, [:name])
    |> validate_required([:name])
  end
end
