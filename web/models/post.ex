defmodule BookSearch.Post do
  use BookSearch.Web, :model

  schema "posts" do
    field :title, :string
    belongs_to :user, BookSearch.User

    timestamps()
  end

  @doc """
  Builds a changeset based on the `struct` and `params`.
  """
  def changeset(struct, params \\ %{}) do
    struct
    |> cast(params, [:title])
    |> validate_required([:title])
  end
end
