defmodule BookSearch.BookController do
  use BookSearch.Web, :controller

  require Logger

  alias BookSearch.Book
  alias BookSearch.Author
  alias BookSearch.Category
  alias BookSearch.Repo

  #plug :action

  def index(conn, _params) do
    books = Book |> Repo.all |> Repo.preload([:categories]) |> Repo.preload([:author])
    render(conn, "index.html", books: books)
  end

  def new(conn, params) do
    Logger.info "author_id #{params["author_id"]}"
    changeset = Book.changeset(%Book{categories: []})
    authors =
      if (params["author_id"]) do
        Repo.all(from(a in Author, select: {a.name, a.id}, where: a.id == ^params["author_id"]))
      else
        authors()
      end
    render(conn, "new.html", changeset: changeset, author: params["author_id"], authors: authors, categories: categories(), category_values: %{})
  end

  def create(conn, %{"book" => book_params}) do
    changeset = Book.changeset(%Book{categories: []}, book_params)
    case Repo.insert(changeset) do
      {:ok, book} ->
        Logger.info "created new book, id: #{book.id}"

        conn
        |> put_flash(:info, "Book created successfully.")
        |> redirect(to: book_path(conn, :index))
      {:error, changeset} ->
        render(conn, "new.html", changeset: changeset, author: nil, authors: authors(), categories: categories())
    end
  end

  def show(conn, %{"id" => id}) do
    book = Repo.get!(Book, id) |> Repo.preload([:categories]) |> Repo.preload([:author])
    render(conn, "show.html", book: book) #, author: author)
  end

  def edit(conn, %{"id" => id}) do
    Logger.info "from_show #{conn.params["from_show"]}"
    book = Repo.get!(Book, id) |> Repo.preload([:categories])
    Logger.info "editing book, id: #{book.id} categories: #{length(book.categories)}"

    changeset = Book.changeset(book)
    render(conn, "edit.html", book: book, changeset: changeset, authors: authors(), categories: categories(), from_show: conn.params["from_show"])
  end

  def update(conn, %{"id" => id, "book" => book_params}) do
    book = Repo.get!(Book, id) |> Repo.preload(:categories)

    changeset = Book.changeset(book, book_params)
    case Repo.update(changeset) do
      {:ok, book} ->

        Logger.info "update book, id: #{book.id}"
        conn
        |> put_flash(:info, "Book updated successfully.")
        |> redirect(to: book_path(conn, :show, book))
      {:error, changeset} ->
        render(conn, "edit.html", book: book, changeset: changeset, authors: authors(), categories: categories(), from_show: conn.params["from_show"])
    end
  end

  def delete(conn, %{"id" => id}) do
    book = Repo.get!(Book, id)

    # Here we use delete! (with a bang) because we expect
    # it to always work (and if it does not, it will raise).
    Repo.delete!(book)

    conn
    |> put_flash(:info, "Book deleted successfully.")
    |> redirect(to: book_path(conn, :index))
  end

  # All categories
  defp categories, do: Repo.all(from(a in Category, select: {a.name, a.id}, order_by: a.name))

  # All authors
  defp authors, do: Repo.all(from(a in Author, select: {a.name, a.id}))

end
