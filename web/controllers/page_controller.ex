defmodule BookSearch.PageController do
  use BookSearch.Web, :controller

  require Logger
  import Ecto.Query
  alias BookSearch.Book
  alias BookSearch.Author

  def index(conn, _params) do
    render conn, "index.html"
  end

  def show_search(conn, _params) do
    conn
    |> render("search.html", books: [], authors: [])
  end

  def search(conn, %{"search" => %{"search_term" => search_term}}) do
    Logger.info "in Search - search term: %#{String.trim(search_term)}%"
    # do a search!
    like_term = "%#{String.trim(search_term)}%"
    books = Repo.all(from b in Book,
    left_join: c in assoc(b, :categories),
    join: a in assoc(b, :author),
    where: like(c.name, ^like_term) or like(b.title, ^like_term) or like(b.isbn, ^like_term) or like(b.year, ^like_term) or like(a.name, ^like_term),
    group_by: b.id)
      |> Repo.preload(:author)
      |> Repo.preload(:categories) # search on book title, isbn, year, author, categories
    authors = Repo.all(from a in Author,
    left_join: b in assoc(a, :books),
    where: like(a.name, ^like_term) or like(a.email, ^like_term) or like(b.title, ^like_term),
    group_by: a.id)
      |> Repo.preload(:books) # search on author name/email/books

    Logger.info "Book count #{length(books)}, Author count #{length(authors)}"
    case length(books) + length(authors) do
      0 ->
        conn
        |> put_flash(:error, "No results found!")
      count ->
        conn
        |> put_flash(:info, "#{count} result(s) found!")
    end
    |> render("search.html", books: books, authors: authors)
  end
end
