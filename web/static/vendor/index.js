$( ".author_jumbo" ).hover(
  function() {
    $( this ).addClass( "hover" );
  }, function() {
    $( this ).removeClass( "hover" );
  }
);

$( ".book_jumbo" ).hover(
  function() {
    $( this ).addClass( "hover" );
  }, function() {
    $( this ).removeClass( "hover" );
  }
);

$( ".category_jumbo" ).hover(
  function() {
    $( this ).addClass( "hover" );
  }, function() {
    $( this ).removeClass( "hover" );
  }
);

$( ".search_jumbo" ).hover(
  function() {
    $( this ).addClass( "hover" );
  }, function() {
    $( this ).removeClass( "hover" );
  }
);

$( ".author_jumbo" ).tooltip();
$( ".book_jumbo" ).tooltip();
$( ".category_jumbo" ).tooltip();
$( ".search_jumbo" ).tooltip();
$('.fa-eye').tooltip();
$('.fa-edit').tooltip();
$('.fa-trash').tooltip();
