defmodule BookSearch.Router do
  use BookSearch.Web, :router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_flash
    plug :protect_from_forgery
    plug :put_secure_browser_headers
  end

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/", BookSearch do
    pipe_through :browser # Use the default browser stack

    get "/", PageController, :index
    get "/search", PageController, :show_search
    post "/search", PageController, :search

    resources "/posts", PostController, only: [:index]
    resources "/users", UserController, only: [:index]

    resources "/authors", AuthorController do
       resources "/books", BookController
    end
    resources "/categories", CategoryController
    resources "/books", BookController

  end

  # Other scopes may use custom stacks.
  # scope "/api", BookSearch do
  #   pipe_through :api
  # end
end
