defmodule BookSearch.AuthorTest do
  use BookSearch.ModelCase

  alias BookSearch.Author

  @valid_attrs %{email: "email@summit.com", name: "some content"}
  @invalid_attrs %{}

  test "changeset with valid attributes" do
    changeset = Author.changeset(%Author{}, @valid_attrs)
    assert changeset.valid?
  end

  test "changeset with invalid attributes" do
    changeset = Author.changeset(%Author{}, @invalid_attrs)
    refute changeset.valid?
  end
end
