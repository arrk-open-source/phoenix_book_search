defmodule BookSearch.BookTest do

  use BookSearch.ModelCase

  alias BookSearch.Book
  alias BookSearch.Author
  alias BookSearch.Category
  alias BookSearch.Repo
  alias Ecto.Changeset

  require Logger

  @valid_attrs %{isbn: "some content", title: "some content", year: 42}
  @invalid_attrs %{}

  test "changeset with valid attributes" do
    changeset = Book.changeset(%Book{}, @valid_attrs)
    assert changeset.valid?
  end

  test "changeset with invalid attributes" do
    changeset = Book.changeset(%Book{}, @invalid_attrs)
    refute changeset.valid?
  end

  test "associations insert changeset with valid params" do
    valid_attrs_author = %{name: "Monty", email: "monty@yahoo.com"}
    valid_attrs_cat1 = %{name: "Sci Fi"}
    valid_attrs_cat2 = %{name: "Horror"}

    author_cs = Author.changeset(%Author{}, valid_attrs_author)
    {:ok, author} = Repo.insert(author_cs)
    Logger.info "new author id: #{author.id}, author: #{author.name}, email: #{author.email}"

    valid_attrs2 = %{isbn: "FGHTY-45673", title: "Harry Plopper", year: 1998, author_id: author.id}
    changeset = Book.changeset(%Book{}, valid_attrs2)
    assert changeset.valid?
    {:ok, book} = Repo.insert(changeset)

    book = book |> Repo.preload(:author)
    book = book |> Repo.preload(:categories)
    Logger.info "new book id: #{book.id}, title: #{book.title}, year: #{book.year}, isbn: #{book.isbn}, author: #{book.author.name}"

    cat1_cs = Category.changeset(%Category{}, valid_attrs_cat1)
    cat2_cs = Category.changeset(%Category{}, valid_attrs_cat2)
    {:ok, cat1} = Repo.insert(cat1_cs)
    {:ok, cat2} = Repo.insert(cat2_cs)

    #changeset = Ecto.Changeset.change(book) |> Ecto.Changeset.put_assoc(:categories, [cat1,cat2])
    changeset = book
                  |> Ecto.Changeset.change
                  |> Ecto.Changeset.put_assoc(:categories, [cat1,cat2])

    Logger.info "changeset.valid? #{changeset.valid?}"

    {:ok, book} = Repo.update(changeset)

    Logger.info "book.categories count #{length(book.categories)}"

    for category <- book.categories do
        Logger.info "#{category.id}, #{category.name}"
    end
  end

  test "can associate existing book with existing category" do
    author = Repo.insert!(%Author{name: "Auth1", email: "someemail@e.com"})
    book = Repo.insert!(%Book{isbn: "FGHTY-45673", title: "Harry Trotter", year: 1998, author_id: author.id}) |> Repo.preload(:categories)
    category = Repo.insert!(%Category{name: "Snow"})
    changeset = book
                |> Changeset.change
                |> Changeset.put_assoc(:categories, [category])
    assert changeset.valid?
    book = Repo.update! changeset
    Logger.info "book id: #{book.id}"
    assert book
  end

  test "can change association for existing book with existing category using struct reference for book - add category" do
    author = Repo.insert!(%Author{name: "Auth1", email: "someemail@e.com"})
    book = Repo.insert!(%Book{isbn: "FGHTY-45673", title: "Harry Trotter", year: 1998, author_id: author.id}) |> Repo.preload(:categories)
    category = Repo.insert!(%Category{name: "Snow"})
    category2 = Repo.insert!(%Category{name: "Ice"})

    changeset = book
                |> Changeset.change
                |> Changeset.put_assoc(:categories, [category])
    assert changeset.valid?
    book = Repo.update! changeset
    Logger.info "book id: #{book.id}"
    Logger.info "categories: #{length(book.categories)}"
    assert length(book.categories) == 1
    assert book

    changeset = %Book{id: book.id}
                |> Repo.preload(:categories)
                |> Changeset.change
                |> Changeset.put_assoc(:categories, [category, category2])
    assert changeset.valid?
    book = Repo.update! changeset
    Logger.info "categories: #{length(book.categories)}"
    assert length(book.categories) == 2
    assert book
  end

  test "can change association for existing book with existing category using struct reference for book - change category" do
    author = Repo.insert!(%Author{name: "Auth1", email: "someemail@e.com"})
    book = Repo.insert!(%Book{isbn: "FGHTY-45673", title: "Harry Trotter", year: 1998, author_id: author.id}) |> Repo.preload(:categories)
    category = Repo.insert!(%Category{name: "Snow"})
    category2 = Repo.insert!(%Category{name: "Ice"})

    changeset = book
                |> Changeset.change
                |> Changeset.put_assoc(:categories, [category])
    assert changeset.valid?
    book = Repo.update! changeset
    Logger.info "book id: #{book.id}"
    Logger.info "categories: #{length(book.categories)}"
    assert length(book.categories) == 1
    assert book

    changeset = %Book{id: book.id}
                |> Repo.preload(:categories)
                |> Changeset.change
                |> Changeset.put_assoc(:categories, [category2])
    assert changeset.valid?
    book = Repo.update! changeset
    Logger.info "categories: #{length(book.categories)}"
    assert length(book.categories) == 1
    assert book
  end
end
