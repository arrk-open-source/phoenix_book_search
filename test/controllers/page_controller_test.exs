defmodule BookSearch.PageControllerTest do
  use BookSearch.ConnCase
  alias BookSearch.{Book, Author}

  setup do
    author = Repo.insert!(%Author{name: "JK Rowling"})
    Repo.insert!(%Author{name: "Dick King Smith", email: "dks@yahoo.com"})
    Repo.insert!(%Book{isbn: "FGHTY-45673", title: "Harry Trotter", year: 1998, author_id: author.id})
    {:ok, conn: build_conn() }
  end

  test "search on page load", %{conn: conn} do
    conn = get conn, "/search"
    assert html_response(conn, 200) =~ "Search books and authors"
  end

  test "search with empty search", %{conn: conn} do
    conn = post conn, "/search", search: %{search_term: ""}
    assert html_response(conn, 200) =~ "Harry Trotter"
    assert html_response(conn, 200) =~ "JK Rowling"
    assert html_response(conn, 200) =~ "3 result(s) found"
  end

  test "search author email", %{conn: conn} do
    conn = post conn, "/search", search: %{search_term: "dks"}
    assert html_response(conn, 200) =~ "Dick King Smith"
    assert html_response(conn, 200) =~ "1 result(s) found"
  end

  test "search with valid book", %{conn: conn} do
    conn = post conn, "/search", search: %{search_term: "Harry"}
    assert html_response(conn, 200) =~ "Harry Trotter"
    assert html_response(conn, 200) =~ "2 result(s) found"
  end

  test "search with valid book white spaces", %{conn: conn} do
    conn = post conn, "/search", search: %{search_term: "   Harry "}
    assert html_response(conn, 200) =~ "Harry Trotter"
    assert html_response(conn, 200) =~ "2 result(s) found"
  end

  test "search with valid author", %{conn: conn} do
    conn = post conn, "/search", search: %{search_term: "Rowling"}
    assert html_response(conn, 200) =~ "Rowling"
    assert html_response(conn, 200) =~ "2 result(s) found"
  end

  test "search with invalid data", %{conn: conn} do
    conn = post conn, "/search", search: %{search_term: "Brown"}
    assert html_response(conn, 200) =~ "No results found"
  end
end
