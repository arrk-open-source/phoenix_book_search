<a class="brand" href="http://www.arrkgroup.com/">
<img src="http://cl.ly/image/1m1A2w040x29/arrklogo.png" alt="Arrk Group">
</a>

Arrk Group http://www.arrkgroup.com 

# Introduction

Arrk is committed to evaluating and investing in future technology. As part of that commitment we perform proof-of-concept evaluations. Here we present such a proof-of-concept: a simple Book Search/Storage application using the Elixir programming language with the Phoenix web framework.

# BookSearch

To start your Phoenix app:

  * Install dependencies with `mix deps.get`
  * Create and migrate your database with `mix ecto.create`
  * Migrate your database with `mix ecto.migrate`
  * (optional) Populate your database with `mix run priv\repo\seeds.exs`
  * Install Node.js dependencies with `npm install --save sass-brunch copycat-brunch bootstrap-sass jquery font-awesome`
  * Start Phoenix endpoint with `mix phoenix.server`

Now you can visit [`localhost:4000`](http://localhost:4000) from your browser.

Ready to run in production? Please [check our deployment guides](http://www.phoenixframework.org/docs/deployment).

## Learn more

  * Official website: http://www.phoenixframework.org/
  * Guides: http://phoenixframework.org/docs/overview
  * Docs: https://hexdocs.pm/phoenix
  * Mailing list: http://groups.google.com/group/phoenix-talk
  * Source: https://github.com/phoenixframework/phoenix
