SELECT b.id, b.title, b.isbn, a.name, a.email, c.name FROM books b
join authors a on b.author_id = a.id
left join bookcategories bc on bc.book_id = b.id
left join categories c on c.id = bc.category_id;

SELECT * FROM books b;
