run app:

in project directory:
> mix phoenix.server

project will be running here: http://localhost:4000/

all files in controller/templates/views is for the hello world app

By convention, in the dev environment, everything in the web folder is recompiled when there is a new web request.

Files in the lib folder won't be recompiled when there is a new web request.

WEB - anything whose state lasts duration of a web request

LIB - shared modules and anything that needs to manage state outside of the duration of a web request

In our controller: 'render conn, "index.html"', Phoenix will look for a index.html.eex in web/templates/hello

template files are .eex which stands for embedded elixir

compile code like this, at root of project
> mix compile

view the routes
> mix phoenix.routes

maps full CRUD pages in phoenix
> resources "/users", UserController

> mix phoenix.routes
 page_path  GET     /                  HelloPhoenix3.PageController :index
hello_path  GET     /hello             HelloPhoenix3.HelloController :index
hello_path  GET     /hello/:messenger  HelloPhoenix3.HelloController :show
 user_path  GET     /users             HelloPhoenix3.UserController :index
 user_path  GET     /users/:id/edit    HelloPhoenix3.UserController :edit
 user_path  GET     /users/new         HelloPhoenix3.UserController :new
 user_path  GET     /users/:id         HelloPhoenix3.UserController :show
 user_path  POST    /users             HelloPhoenix3.UserController :create
 user_path  PATCH   /users/:id         HelloPhoenix3.UserController :update
            PUT     /users/:id         HelloPhoenix3.UserController :update
 user_path  DELETE  /users/:id         HelloPhoenix3.UserController :delete
 
add a link in view to a path:
 <a href="<%= hello_path(@conn, :show, "Aliya") %>">CLICK HERE TO SEE WELCOME</a>
 
##########################
# Run iex and view paths
##########################
1. in cmd, in project root
> iex -S mix
iex> import HelloPhoenix3.Router.Helpers
iex> alias HelloPhoenix3.Endpoint
iex> hello_path(Endpoint, :index)
"/hello"
iex> hello_path(Endpoint, :show, "Aliya")
"/hello/Aliya"

USE '_url' instead of '_path' if whole URL is needed

iex> hello_url(Endpoint, :show, "Aliya", print: true)
"http://localhost:4000/hello/Aliya?print=true"

ADD extra key values on the end, of parameters are wanted in the url>

iex> hello_path(Endpoint, :show, "Aliya", print: true)
"hello/Aliya?print=true"

Stuff in /priv/static, get auto generated

#########
# Plug
#########

Endpoints, Routers, Controller are all just Plugs internally
Plug is a specification for composable modules in between web applications
Two types: function plugs and module plugs
Function plugs can do things like:
plug :authenticate
plug :find_message
plug :authorise_message

Can use plugs for most things - reusable

###############
# ECTO models
###############
example - generate resources for database
> mix phoenix.gen.html Author authors name:string email:string

example, references:
> mix phoenix.gen.html Post posts title body:text
> mix phoenix.gen.html Comment comments name content post_id:references:posts

add resource to routes
> resources "/authors", AuthorController

creates the database
> mix ecto.create
update repository by running migration, this builds the database
> mix ecto.migrate

using iex eshell to check models:
> iex -S mix
> alias BookSearch.Author
> changeset = Author.changeset(%Author{}, %{})
> changeset.valid?

when add a new field in, will need to do a migration:
> mix ecto.gen.migration add_field_to_books

using console to create new records on database:
> iex -S mix
> alias BookSearch.Repo
> alias BookSearch.Book
> Repo.insert(%Book{title: "BFG", year: 1988, author_id: 1, isbn: "QWETYY-2323"})
[debug] QUERY OK db=0.0ms
begin []
[debug] QUERY OK db=0.0ms
INSERT INTO `books` (`author_id`,`isbn`,`title`,`year`,`inserted_at`,`updated_at`) VALUES (?,?,?,?,?,?) [1, "QWETYY-2323", "BFG", 1988, {{2017, 2, 14}, {16, 3, 28, 144000}}, {{2017, 2, 14}, {16, 3, 28, 144000}}]
[debug] QUERY OK db=15.0ms
commit []
{:ok,
 %BookSearch.Book{__meta__: #Ecto.Schema.Metadata<:loaded, "books">,
  author: #Ecto.Association.NotLoaded<association :author is not loaded>,
  author_id: 1, id: 5, inserted_at: ~N[2017-02-14 16:03:28.144000],
  isbn: "QWETYY-2323", title: "BFG", updated_at: ~N[2017-02-14 16:03:28.144000],
  year: 1988}}
  
Relationships: Many-To-One
Author has many books
Book has one author
Will need to preload the books for author.books to work

> iex -S mix
> alias BookSearch.Author
> alias BookSearch.Repo
> alias BookSearch.Book
> import Ecto.Query
> author = Repo.get_by!(Author, name: "JK Rowling")
> author = Repo.preload(author, :books)
> author.books

##############################################
# Setup jQuery and Bootstap sass font awesome
http://cloudless.studio/articles/4-installing-font-awesome-from-npm-in-phoenix

##############################################
install packages:
npm install --save font-awesome bootstrap-sass sass-brunch jquery

brunch-config.js

add to plugins section:
sass: {
      options: {
        includePaths: ["node_modules/bootstrap-sass/assets/stylesheets"], // tell sass-brunch where to look for files to @import
        precision: 8 // minimum precision required by bootstrap-sass
      }
    },

updated npm section:	
npm: {
    enabled: true,
    whitelist: ["phoenix", "phoenix_html", "jquery", "bootstrap-sass"], // pull jquery and bootstrap-sass in as front-end assets
    globals: {
      $: 'jquery',
      jQuery: 'jquery',
      bootstrap: 'bootstrap-sass' // require bootstrap-sass' JavaScript globally
    }
  }

package.json:
"dependencies": {
    "bootstrap": "^3.3.5",
    "bootstrap-sass": "^3.3.7",
    "copycat-brunch": "^1.1.0",
    "jquery": "^3.1.1",
    "phoenix": "file:deps/phoenix",
    "phoenix_html": "file:deps/phoenix_html",
    "sass-brunch": "^2.10.4"
  },
  "devDependencies": {
    "babel-brunch": "~6.0.0",
    "brunch": "2.7.4",
    "clean-css-brunch": "~2.0.0",
    "coffee-script-brunch": ">= 1.8",
    "copycat-brunch": "^1.1.0",
    "css-brunch": "~2.0.0",
    "javascript-brunch": "~2.0.0",
    "jquery": ">= 2.1",
    "sass-brunch": "^2.10.4",
    "uglify-js-brunch": "~2.0.1"
  }





  
################
# Elixir notes
################

All data is immutable. So for example:
array = [1,2,3]
some_function(array)
print(array)

array will always be the same. This fixes the multi threading problem. never have to worry about other threads changing it

Mutability leaves us with, 'how did I get to this state?'

Immutable data is known data. We don't hack data, we transform it into something new.

No garbage collection is required. 

Concatenation: 

iex> name = "one"
iex> name2 = "two"
iex> name3 = name <> name2
"onetwo"






