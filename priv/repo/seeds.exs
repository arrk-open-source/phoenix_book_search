# Script for populating the database. You can run it as:
#
#     mix run priv\repo\seeds.exs
#     if db is already in use, can drop, create, migrate before running this
#     mix ecto.drop
#     mix ecto.create
#     mix ecto.migrate
# Inside the script, you can read and write to any of your
# repositories directly:
#
#     BookSearch.Repo.insert!(%BookSearch.SomeModel{})
#
# We recommend using the bang functions (`insert!`, `update!`
# and so on) as they will fail if something goes wrong.

# add default categories
{:ok, fictionCategory} = BookSearch.Repo.insert(%BookSearch.Category{name: "Fiction"})
{:ok, scifiCategory} = BookSearch.Repo.insert(%BookSearch.Category{name: "Sci-Fi"})
{:ok, horrorCategory} = BookSearch.Repo.insert(%BookSearch.Category{name: "Horror"})
{:ok, fantasyCategory} = BookSearch.Repo.insert(%BookSearch.Category{name: "Fantasy"})
{:ok, romanceCategory} = BookSearch.Repo.insert(%BookSearch.Category{name: "Romance"})
{:ok, bioCategory} = BookSearch.Repo.insert(%BookSearch.Category{name: "Biography"})
{:ok, actionCategory} = BookSearch.Repo.insert(%BookSearch.Category{name: "Action"})
{:ok, thrillerCategory} = BookSearch.Repo.insert(%BookSearch.Category{name: "Thriller"})
{:ok, comedyCategory} = BookSearch.Repo.insert(%BookSearch.Category{name: "Comedy"})

# add some authors
{:ok, jkRowling} = BookSearch.Repo.insert(%BookSearch.Author{name: "JK Rowling", email: "jkrowling@yahoo.com"})
{:ok, roaldDahl} = BookSearch.Repo.insert(%BookSearch.Author{name: "Roald Dahl", email: "roalddahl@yahoo.com"})
{:ok, danBrown} = BookSearch.Repo.insert(%BookSearch.Author{name: "Dan Brown"})
{:ok, dickKingSmith} = BookSearch.Repo.insert(%BookSearch.Author{name: "Dick King Smith"})

# add some books
BookSearch.Repo.insert!(%BookSearch.Book{title: "Harry Potter and the Philosopher's Stone", isbn: "ERTDF-4564", year: 1997, author: jkRowling, categories: [fictionCategory,fantasyCategory]})
BookSearch.Repo.insert!(%BookSearch.Book{title: "Harry Potter and the Chamber of Secrets", isbn: "ERTDF-3456", year: 1998, author: jkRowling, categories: [fictionCategory,fantasyCategory]})
BookSearch.Repo.insert!(%BookSearch.Book{title: "Harry Potter and the Prisoner of Azkaban", isbn: "ERTDF-6789", year: 1999, author: jkRowling, categories: [fictionCategory,fantasyCategory]})
BookSearch.Repo.insert!(%BookSearch.Book{title: "Harry Potter and the Goblet of Fire", isbn: "ERTDF-4567", year: 2001, author: jkRowling, categories: [fictionCategory,fantasyCategory]})
BookSearch.Repo.insert!(%BookSearch.Book{title: "Harry Potter and the Order of the Phoenix", isbn: "ERTDF-1234", year: 2003, author: jkRowling, categories: [fictionCategory,fantasyCategory]})
BookSearch.Repo.insert!(%BookSearch.Book{title: "Harry Potter and the Half-Blood Prince", isbn: "ERTDF-2345", year: 2005, author: jkRowling, categories: [fictionCategory,fantasyCategory,horrorCategory]})
BookSearch.Repo.insert!(%BookSearch.Book{title: "Harry Potter and the Deathly Hallows", isbn: "ERTDF-4321", year: 2007, author: jkRowling, categories: [fictionCategory,fantasyCategory,horrorCategory]})
BookSearch.Repo.insert!(%BookSearch.Book{title: "Matilda", isbn: "DAHL-43434", year: 1988, author: roaldDahl, categories: [fictionCategory,fantasyCategory]})
BookSearch.Repo.insert!(%BookSearch.Book{title: "BFG", isbn: "DAHL-63434", year: 1988, author: roaldDahl, categories: [fictionCategory,fantasyCategory]})
BookSearch.Repo.insert!(%BookSearch.Book{title: "The Da Vinci Code", isbn: "ERTDF-3434", year: 2003, author: danBrown, categories: [fictionCategory,thrillerCategory]})
BookSearch.Repo.insert!(%BookSearch.Book{title: "Inferno", isbn: "ERTDF-7890", year: 2006, author: danBrown, categories: [fictionCategory,thrillerCategory]})
BookSearch.Repo.insert!(%BookSearch.Book{title: "Magnus Powermouse", isbn: "DKS-22-7890", year: 1996, author: dickKingSmith, categories: [fictionCategory,fantasyCategory]})
