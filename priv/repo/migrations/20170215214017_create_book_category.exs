defmodule BookSearch.Repo.Migrations.CreateBookCategory do
  use Ecto.Migration

  def change do
    create table(:bookcategories, primary_key: false) do
      add :book_id, references(:books, on_delete: :delete_all), primary_key: true
      add :category_id, references(:categories, on_delete: :delete_all), primary_key: true
    end
    create index(:bookcategories, [:book_id])
    create index(:bookcategories, [:category_id])

  end
end
